package recoverytrackerapi.util;

import recoverytrackerapi.dto.MedDataDto;
import recoverytrackerapi.model.MedData;

public class Transformer {

    public static MedData toMedDataModel(MedDataDto initialObject) {
        if (initialObject == null) {
            return null;
        }

        MedData resultingObject = new MedData();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUser_id(initialObject.getUser_id());
        resultingObject.setDate(initialObject.getDate());
        resultingObject.setTemp(initialObject.getTemp());
        resultingObject.setSolids(initialObject.getSolids());
        resultingObject.setLiquids(initialObject.getLiquids());
        resultingObject.setExcrement(initialObject.getExcrement());
        resultingObject.setUrine(initialObject.getUrine());
        resultingObject.setVomit(initialObject.getVomit());
        resultingObject.setComments(initialObject.getComments());
        return resultingObject;
    }

//    public static MedDataDto toMedDataDto(MedData initialObject) {
//        if (initialObject == null) {
//            return null;
//        }
//    }
//        MedDataDto resultingObject = new MedDataDto();
//        resultingObject.setId(initialObject.getId());
//        resultingObject.setName(initialObject.getName());
//        resultingObject.setLogo(initialObject.getLogo());
//        resultingObject.setEstablished(initialObject.getEstablished());
//        resultingObject.setEmployees(initialObject.getEmployees());
//        resultingObject.setRevenue(initialObject.getRevenue());
//        resultingObject.setNetIncome(initialObject.getNetIncome());
//        resultingObject.setSecurities(initialObject.getSecurities());
//        resultingObject.setSecurityPrice(initialObject.getSecurityPrice());
//        resultingObject.setDividends(initialObject.getDividends());
//
//        if (initialObject.getSecurities() > 0 && initialObject.getSecurityPrice() > 0) {
//            Double marketCapitalization = Helper.round(initialObject.getSecurities() * initialObject.getSecurityPrice());
//            resultingObject.setMarketCapitalization(marketCapitalization);
//        }
//
//        if (initialObject.getSecurityPrice() > 0 && initialObject.getDividends() > 0) {
//            Double dividendYield = Helper.round(initialObject.getDividends() / initialObject.getSecurityPrice());
//            resultingObject.setDividendYield(dividendYield);
//        }
//
//        return resultingObject;
//    }

}
