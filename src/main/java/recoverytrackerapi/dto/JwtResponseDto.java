package recoverytrackerapi.dto;

public class JwtResponseDto {

    private final int id;
    private final String username;
    private final String token;
    private final String logo;

    public JwtResponseDto(int id, String username, String token, String logo) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.logo = logo;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return this.token;
    }
    public String getLogo() { return  this.logo; }
}
