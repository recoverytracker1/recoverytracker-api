package recoverytrackerapi.repository;

import recoverytrackerapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //see on kontroll, kas selle kasutajanimega kasutaja on olemas:
    //lisame pärast ka kasutaja parooli muutmise funktsiooni
    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }

    //esialgu eeldame, et admin rolliga kasutaja saab teha vaid DB admin ja veebist vaid rolliga user
    public void addUser(User user) {
        jdbcTemplate.update("insert into `user` (`user_role`, `username`, `personal_code`, `parent_id`, `first_name`, `last_name`, `password`, `logo`) " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?)", "user", user.getUsername(), user.getPersonal_code(), user.getParent_id(), user.getFirst_name(),
                user.getLast_name(), user.getPassword(), user.getLogo());
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("user_role"), rs.getString("username"), rs.getString("personal_code"), rs.getInt("parent_id"),
                        rs.getString("first_name"), rs.getString("last_name"), rs.getString("password"), rs.getString("logo")));
        return users.size() > 0 ? users.get(0) : null;
    }

    public void updateUser(User user) {
        jdbcTemplate.update("update `user` set `username` = ? where id = ?", user.getUsername(), user.getId());
    }


    public List<User> getChildren(int parentId) {
        return jdbcTemplate.query(
                "select * from `user` where `parent_id` = ?",
                new Object[]{parentId},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("user_role"), rs.getString("username"), rs.getString("personal_code"), rs.getInt("parent_id"),
                        rs.getString("first_name"), rs.getString("last_name"), rs.getString("password"), rs.getString("logo")));
    }

    public List<User> getUserData(String userName) {
        return jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                new Object[]{userName},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("user_role"), rs.getString("username"), rs.getString("personal_code"), rs.getInt("parent_id"),
                        rs.getString("first_name"), rs.getString("last_name"), rs.getString("password"), rs.getString("logo")));
    }
}
