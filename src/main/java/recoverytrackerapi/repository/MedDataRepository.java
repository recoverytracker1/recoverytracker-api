package recoverytrackerapi.repository;

import recoverytrackerapi.model.MedData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Repository
public class MedDataRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<MedData> getMedInfo() {
        return jdbcTemplate.query("select * from med_data", mapMedDataRows);
    }

    public MedData getMedData(int id) {
        List<MedData> medInfo = jdbcTemplate.query("select * from med_data where id = ?", new Object[]{id}, mapMedDataRows);
        return medInfo.size() > 0 ? medInfo.get(0) : null;
    }

    //SELECT * FROM med_data WHERE user_id IN (SELECT id FROM user WHERE username = "postman3");
    public List<MedData> getMedData(String username) {
        return jdbcTemplate.query("SELECT * FROM med_data WHERE user_id IN (SELECT id FROM user WHERE username = ?)",
                new Object[]{username}, mapMedDataRows);
    }

    public boolean medDataExists(MedData medData) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from med_data where user_id = ?", new Object[]{medData.getUser_id()}, Integer.class);
        return count != null && count > 0;
    }

//    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//    LocalDateTime myPcTime = LocalDateTime.now();
    public void addMedData(MedData medData) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime myPcTime = LocalDateTime.now();
        jdbcTemplate.update(
                "insert into med_data (user_id, date, temp, solids, liquids,  excrement, urine, vomit, comments) values (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                medData.getUser_id(), myPcTime, medData.getTemp(), medData.getSolids(), medData.getLiquids(), medData.getExcrement(),
                medData.getUrine(), medData.getVomit(), medData.getComments()
        );
    }

    public void updateMedData(MedData medData) {
        jdbcTemplate.update(
                "update med_data set user_id = ?, date = ?, temp = ?, solids = ?, liquids = ?,  excrement = ?, urine = ?, vomit = ?, comments = ? where id = ?",
                medData.getUser_id(), medData.getDate(), medData.getTemp(), medData.getSolids(), medData.getLiquids(), medData.getExcrement(),
                medData.getUrine(), medData.getVomit(), medData.getComments(), medData.getId()
        );
    }

    public void deleteMedData(int id) {jdbcTemplate.update("delete from med_data where id = ?", id); }

    private RowMapper<MedData> mapMedDataRows = (rs, rowNum) -> {
        MedData medData = new MedData();
        medData.setId(rs.getInt("id"));
        medData.setUser_id(rs.getInt("user_id"));
        medData.setDate(rs.getTimestamp("date") != null ? rs.getTimestamp("date").toLocalDateTime() : null);
        medData.setTemp(rs.getFloat("temp"));
        medData.setSolids(rs.getString("solids"));
        medData.setLiquids(rs.getString("liquids"));
        medData.setExcrement(rs.getString("excrement"));
        medData.setUrine(rs.getString("urine"));
        medData.setVomit(rs.getString("vomit"));
        medData.setComments(rs.getString("comments"));
        return medData;
    };
}
