package recoverytrackerapi.model;

import java.time.LocalDateTime;

public class MedData {


    private Integer id;
    private Integer user_id;
    private LocalDateTime date;
    private Float temp;
    private String solids;
    private String liquids;
    private String excrement;
    private String urine;
    private String vomit;
    private String comments;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Float getTemp() {
        return temp;
    }

    public void setTemp(Float temp) {
        this.temp = temp;
    }

    public String getSolids() {
        return solids;
    }

    public void setSolids(String solids) {
        this.solids = solids;
    }

    public String getLiquids() {
        return liquids;
    }

    public void setLiquids(String liquids) {
        this.liquids = liquids;
    }

    public String getExcrement() {
        return excrement;
    }

    public void setExcrement(String excrement) {
        this.excrement = excrement;
    }

    public String getUrine() {
        return urine;
    }

    public void setUrine(String urine) {
        this.urine = urine;
    }

    public String getVomit() {
        return vomit;
    }

    public void setVomit(String vomit) {
        this.vomit = vomit;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
