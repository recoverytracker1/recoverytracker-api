package recoverytrackerapi.model;

public class User {

    private int id;
    private String user_role;
    private String username;
    private String personal_code;
    private int parent_id;
    private String first_name;
    private String last_name;
    private String password;
    private String logo;

    public User(int id, String user_role, String username, String personal_code, int parent_id, String first_name, String last_name, String password, String logo) {
        this.id = id;
        this.user_role = user_role;
        this.username = username;
        this.personal_code = personal_code;
        this.parent_id = parent_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
        this.logo = logo;
    }

    public User(String user_role, String username, String personal_code, int parent_id, String first_name, String last_name, String password, String logo) {
        this.user_role = user_role;
        this.username = username;
        this.personal_code = personal_code;
        this.parent_id = parent_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
        this.logo = logo;
    }

//    public User(int id, String username, String password) {
//        this.id = id;
//        this.username = username;
//        this.password = password;
//    }
//
//    public User(String username, String password) {
//        this.username = username;
//        this.password = password;
//    }
//


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPersonal_code() {
        return personal_code;
    }

    public void setPersonal_code(String personal_code) {
        this.personal_code = personal_code;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


}
