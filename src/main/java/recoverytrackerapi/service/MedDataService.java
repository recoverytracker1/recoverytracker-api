package recoverytrackerapi.service;

import org.springframework.security.core.context.SecurityContextHolder;
import recoverytrackerapi.dto.MedDataDto;
import recoverytrackerapi.model.MedData;
import recoverytrackerapi.model.User;
import recoverytrackerapi.repository.MedDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import recoverytrackerapi.repository.UserRepository;
import recoverytrackerapi.util.Transformer;

import java.time.LocalDate;
import java.util.List;

@Service
public class MedDataService {

    @Autowired
    private MedDataRepository medDataRepository;

    @Autowired
    private UserRepository userRepository;

    public List<MedData> getMedInfo() {
        return medDataRepository.getMedInfo();
    }

    public MedData getMedData(int id) {
        Assert.isTrue(id > 0, "The ID of the patient is not specified");

        MedData medData = medDataRepository.getMedData(id);
        return  medDataRepository.getMedData(id);
    }

    public List<MedData> getMedData(String username) {
        //Assert.isTrue(id > 0, "The ID of the patient is not specified");
        List<MedData> medData = medDataRepository.getMedData(username);
        return  medData;
    }

    public void savedData(MedData medData) {
        Assert.notNull(medData, "Data is not specified");

    }


    public void deleteMedData(int id) {
        if (id > 0) {
            medDataRepository.deleteMedData(id);
        }
    }

    public void saveMedData(MedDataDto medDataDto) {
        MedData medData = Transformer.toMedDataModel(medDataDto);
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getUserByUsername(authenticatedUsername);
        medData.setUser_id(user.getId());
        medDataRepository.addMedData(medData);
    }

    public void putMedInfo(MedDataDto medDataDto) {
        MedData medData = Transformer.toMedDataModel(medDataDto);
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getUserByUsername(authenticatedUsername);
        medData.setUser_id(user.getId());
        medDataRepository.updateMedData(medData);
    }
}
