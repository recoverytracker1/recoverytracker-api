package recoverytrackerapi.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;
import recoverytrackerapi.dto.GenericResponseDto;
import recoverytrackerapi.dto.JwtRequestDto;
import recoverytrackerapi.dto.JwtResponseDto;
import recoverytrackerapi.dto.UserRegistrationDto;
import recoverytrackerapi.model.User;
import recoverytrackerapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import recoverytrackerapi.util.Transformer;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public GenericResponseDto register(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        User user = new User(userRegistration.getUser_role(), userRegistration.getUsername(), userRegistration.getPersonal_code(), userRegistration.getParent_id(),
                userRegistration.getFirst_name(), userRegistration.getLast_name(), passwordEncoder.encode(userRegistration.getPassword()), userRegistration.getLogo());
        if (!userRepository.userExists(userRegistration.getUsername())) {
            userRepository.addUser(user);
        } else {
            responseDto.getErrors().add("User with the specified username already exists.");
        }
        return responseDto;
    }


    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        final User userDetails = userRepository.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token, userDetails.getLogo()); // logo kaasa
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public List<User> getChildren() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName(); // annab username
        User currentlyLoggedInUser = userRepository.getUserByUsername(username);
        int parentId = currentlyLoggedInUser.getId();
        return userRepository.getChildren(parentId);
    }

    public List<User> getUserData() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName(); // annab username
        User currentlyLoggedInUser = userRepository.getUserByUsername(username);
        //int parentId = currentlyLoggedInUser.getId();
        return userRepository.getUserData(username);
    }

//    public void updateUser(UserRegistrationDto userRegistrationDto) {
//        Assert.hasText(userRegistrationDto.getUsername(), "User name is not specified");
//        User user = userRepository.getUserByUsername(userRegistrationDto.getUsername());
//        if (user.getUsername() != null) {
//            userRepository.updateUser(user);
//        } else {
//            Assert.isTrue(!userRepository.userExists(user.getUsername()), "The user with the specified name already exists");
//            userRepository.addUser(user);
//        }
//    }



}