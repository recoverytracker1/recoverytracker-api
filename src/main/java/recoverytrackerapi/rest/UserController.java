package recoverytrackerapi.rest;

import recoverytrackerapi.dto.GenericResponseDto;
import recoverytrackerapi.dto.JwtRequestDto;
import recoverytrackerapi.dto.JwtResponseDto;
import recoverytrackerapi.dto.UserRegistrationDto;
import recoverytrackerapi.model.User;
import recoverytrackerapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }


    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
    @GetMapping("/children")
    public List<User> getChidren() { return userService.getChildren(); }

    @GetMapping("/username")
    public List<User> getUserData() { return userService.getUserData(); }
}