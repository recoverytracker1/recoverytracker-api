package recoverytrackerapi.rest;

import recoverytrackerapi.dto.MedDataDto;
import recoverytrackerapi.model.MedData;
import recoverytrackerapi.service.MedDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medinfo")
@CrossOrigin("*")
public class MedDataController {

    @Autowired
    private MedDataService medDataService;

    @GetMapping
    public List<MedData> getMedInfo() {
        return medDataService.getMedInfo();
    }

    @GetMapping("/{id}")
    public MedData getMedInfo(@PathVariable("id") int id) {
        return medDataService.getMedData(id);
    }


    @GetMapping("/username/{username}")
    public List<MedData> getMedInfo(@PathVariable("username") String username) {
        return medDataService.getMedData(username);
    }

    @PostMapping
    public void saveMedData(@RequestBody MedDataDto medDataDto) {
        medDataService.saveMedData(medDataDto);
    }

    @PutMapping
    public void putMedInfo(@RequestBody MedDataDto medDataDto)  {
        medDataService.putMedInfo(medDataDto);
    }


    @DeleteMapping("/{id}")
    public void deleteMedData(@PathVariable("id") int id) {
        medDataService.deleteMedData(id);
    }

}
